import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.createFile
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries

fun main() {
    /*PRIMER EXERCICI DE REPAS*/
    val path: Path = Path("/home/sjo/IdeaProjects/itbRepasKotlin/src");
    val filePath = path.resolve("emptyfile.txt");

    if (!filePath.exists()) {
        filePath.createFile();
    } else {
        println("El fitxer ja existex!")
    }

    val files: List<Path> = path.listDirectoryEntries();
    println("A la carpeta src tenim --> " + files.size + " Carpetes o fitxers")
    println(files)
    val txtFiles: List<Path> = path.listDirectoryEntries("*.txt");
    println("Tenim " + txtFiles.size + " arxius txt a la carpeta")
    println(txtFiles);
    println()
}