import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.util.*

suspend fun main(){
    /*SEGON EXERCICI DE REPAS*/
    val scan = Scanner(System.`in`);
    println("Ben vingut al AgeMagician. Com et dius?")
    val name = scan.next();

    val client = HttpClient(){
        install(ContentNegotiation){
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val result: PersonApi = client.get("https://api.agify.io?name=$name&country_id=ES").body();
    println("Jo crec que tens " +result.age+ " anys!")
}