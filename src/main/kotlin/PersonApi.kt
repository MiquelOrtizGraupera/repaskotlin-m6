import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PersonApi(
    val name: String,
    val age: Int,
    val count: Int,
    @SerialName("country_id") val countryId: String
)
