package paths
import kotlin.io.path.Path
import java.nio.file.Path
import java.util.Scanner
import kotlin.io.path.exists

fun main(){
    println("Write a path to check if it exists")
    val scan = Scanner(System.`in`)
    val input = scan.next()

    val path = Path(input)
    if(path.exists()){
        println(true)
    }else{
        println(false)
    }


}